## About this Project

## Requirements
* PHP7.4
* PostgreSQL10
* nginx
* Docker
* docker-compose

## How to build at local
1. build
    ```bash
   cd /path/to/project
   docker-compose build    
   ```
1. run
   ```bash
   cd /path/to/project
   docker-compose up
   ```
1. install packages
    ```bash
    cd /path/to/project
    docker-compose exec app composer install
    ```
1. migrate
   ```bash
   cd /path/to/project
   docker-compose exec app php artisan migrate --force
   ```
