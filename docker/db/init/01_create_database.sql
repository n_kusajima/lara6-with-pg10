SET client_encoding = 'UTF8';
CREATE DATABASE local_db WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'ja_JP.UTF-8' LC_CTYPE = 'ja_JP.UTF-8';
ALTER DATABASE local_db OWNER TO postgres;
